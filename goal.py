# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import calendar
from datetime import date
from decimal import Decimal

from sql import With
from sql.aggregate import Sum
from sql.conditionals import Case
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, Not
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard

KIND = [
    # ('operation_center', 'Operation Center'),
    ('shop', 'Shop'),
]

INDICATORS = [
    ('salesman', 'Salesman'),
    ('product', 'Product'),
    ('category', 'Category'),
]

TYPE_DICT = {
    'monthly': {'name': 'Monthly', 'range': 1},
    'bimonthly': {'name': 'Bimonthly', 'range': 2},
    'quarterly': {'name': 'Quarterly', 'range': 3},
    'annual': {'name': 'Annual', 'range': 12},
}
TYPE = [(t, v['name']) for t, v in TYPE_DICT.items()]

_ZERO = Decimal(0)


def get_dom_goal_period(start, end):
    dom = ['OR', [
            ('start_date', '>=', start),
            ('end_date', '<=', end),
        ], [
            ('start_date', '>=', start),
            ('start_date', '<=', end),
        ], [
            ('start_date', '<=', start),
            ('end_date', '>=', start),
        ],
    ]
    return dom


class Goal(Workflow, ModelSQL, ModelView):
    "Goal"
    __name__ = 'sale.goal'
    _STATES = {
        'readonly': Eval('state') != 'draft',
    }
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
        required=True, states=_STATES, context={
            'fiscalyear': Eval('fiscalyear'),
        })
    company = fields.Many2One('company.company', 'Company', required=True,
        states=_STATES)
    # places = fields.One2Many('sale.goal.place', 'goal', 'Places',
    #     states=_STATES)
    periods = fields.One2Many('sale.goal.period', 'goal', 'Periods',
        states=_STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
        ], 'State', readonly=True)
    type_ = fields.Selection(TYPE, 'Type', required=True)
    total_amount = fields.Numeric('Total Amount', digits=(16, 2),
        required=True)

    @classmethod
    def __setup__(cls):
        super(Goal, cls).__setup__()
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'open',
            },
            'open': {
                'invisible': Eval('state') != 'draft',
            },
            'close': {
                'invisible': Eval('state') != 'open',
            },
            'fill_periods': {
                'invisible': Eval('state') != 'draft',
            },
        })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'draft'),
            ('open', 'closed'),
        ))

    def get_rec_name(self, name):
        if self.fiscalyear:
            return self.fiscalyear.name

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        pass

    @classmethod
    @ModelView.button
    def fill_periods(cls, records):
        for record in records:
            record.create_periods()

    def create_periods(self):
        Period = Pool().get('sale.goal.period')
        res = []
        currents = [pd.period.id for pd in self.periods]
        for period in self.fiscalyear.periods:
            if period.id in currents:
                continue
            res.append({
                'goal': self.id,
                'period': period.id,
                'total_amount': 0,
            })
        Period.create(res)
        self.save()

    @classmethod
    def get_periods_zeros(cls, fiscalyear):
        periods_zeros = {}
        for p in fiscalyear.periods:
            month = p.name[-2:]
            if month.isdigit() and int(month) >= 1 and int(month) <= 12:
                periods_zeros[month] = _ZERO
        return periods_zeros

    @classmethod
    def get_periods_goal(cls, fiscalyear):
        periods_goal = []
        for p in fiscalyear.periods:
            month = p.name[-2:]
            if month.isdigit() and int(month) >= 1 and int(month) <= 12:
                periods_goal.append(p)
        return periods_goal

    @staticmethod
    def default_total_amount():
        return 0


class GoalPeriod(ModelSQL, ModelView):
    "Goal Period"
    __name__ = 'sale.goal.period'
    goal = fields.Many2One('sale.goal', 'Goal', required=True)
    period = fields.Many2One('account.period', 'Period', required=True)
    total_amount = fields.Numeric('Total Amount', digits=(16, 2),
        required=True)
    places = fields.One2Many('sale.goal.place', 'period', 'Places')

    @classmethod
    def __setup__(cls):
        super(GoalPeriod, cls).__setup__()
        cls._order.insert(0, ('period.name', 'ASC'))

    @staticmethod
    def default_amount():
        return 0


class GoalPlace(ModelSQL, ModelView):
    "Goal Place"
    __name__ = 'sale.goal.place'
    period = fields.Many2One('sale.goal.period', 'Period', required=True)
    kind = fields.Selection(KIND, 'kind', states={'required': True})
    kind_string = kind.translated('kind')
    total_amount = fields.Numeric('Total Amount', digits=(16, 2), required=True)
    amount_by_default = fields.Numeric('Amount By Default', digits=(16, 2))
    indicators = fields.One2Many('sale.goal.indicator', 'place', 'Indicators')
    # shop = fields.Many2One('sale.shop', 'Shop', states={
    #     'required': Eval('kind') == 'shop',
    # })
    # oc = fields.Many2One('company.operation_center', 'OC', states={
    #     'required': Eval('kind') == 'oc',
    # })

    @staticmethod
    def default_total_amount():
        return 0

    # @classmethod
    # def __register__(cls, module_name):
    #     super(GoalPlace, cls).__register__(module_name)
    #     sql_table = cls.__table_handler__(module_name)
    #
    #     if sql_table.column_exist('indicator'):
    #         sql_table.drop_column('indicator')
    #     if sql_table.column_exist('period'):
    #         sql_table.drop_column('period')
    #     if sql_table.column_exist('amount'):
    #         sql_table.column_rename('amount', 'fixed_amount')

    def get_sales_query(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        SaleShop = pool.get('sale.shop')
        Employee = pool.get('company.employee')
        Product = pool.get('product.product')
        Template = pool.get('product.template')
        Party = pool.get('party.party')
        InvoiceTax = pool.get('account.invoice.tax')
        Lang = pool.get('ir.lang')
        lang = Lang.get()

        # Temp_Cat = pool.get('product_template-product_category')
        # Product_Category = pool.get('product_category')

        shop = SaleShop.__table__()
        invoice = Invoice.__table__()
        line = InvoiceLine.__table__()
        employee = Employee.__table__()
        product = Product.__table__()
        template = Template.__table__()
        customer = Party.__table__()
        salesman = Party.__table__()
        invoicetax = InvoiceTax.__table__()
        # tem_cat = Temp_Cat.__table__()
        # prod_cat = Product_Category.__table__()

        data = Transaction().context['data']
        with_ = []
        if not data['with_tax']:
            column_amount = Sum(line.quantity * line.unit_price)
            from_items = line.join(invoice, condition=line.invoice == invoice.id,
                ).join(customer, condition=invoice.party == customer.id,
                ).join(employee, condition=invoice.salesman == employee.id,
                ).join(salesman, condition=employee.party == salesman.id,
                ).join(product, condition=line.product == product.id,
                ).join(template, condition=product.template == template.id,
                ).join(shop, condition=invoice.shop == shop.id,
                )
        elif data['with_tax']:
            from_items = invoice.join(customer, condition=invoice.party == customer.id,
                ).join(employee, condition=invoice.salesman == employee.id,
                ).join(salesman, condition=employee.party == salesman.id,
                ).join(shop, condition=invoice.shop == shop.id,
                )
            columns_invoicetax = [
                    invoicetax.invoice.as_('invoice'),
                    Sum(invoicetax.amount).as_('amount'),
                ]
            query_tax = invoicetax.select(
                *columns_invoicetax,
                group_by=(invoicetax.invoice),
                )
            with_invoicetax = With(query=query_tax)
            column_amount = Sum(invoice.untaxed_amount_cache + with_invoicetax.amount)
            with_ = [with_invoicetax]
            from_items = from_items.join(
                with_invoicetax, 'LEFT',
                condition=with_invoicetax.invoice == invoice.id,
            )

        res = _ZERO
        columns = []
        where = invoice.invoice_date >= data['from_date']
        where &= invoice.state.in_(['posted', 'paid', 'validated'])
        if data['kind'] == 'salesman':
            indicador_ids = [l.salesman.id for l in self.lines]
            where &= employee.id.in_(indicador_ids)
            columns = [salesman.name.as_('indicador'), employee.id.as_('indicador_id')]
            group_by = (salesman.name, employee.id)
        elif data['kind'] == 'product':
            indicador_ids = [l.product.id for l in self.lines]
            where &= product.id.in_(indicador_ids)
            columns = [template.name.as_('indicador'), product.id.as_('indicador_id')]
            group_by = (template.name, product.id)
        elif data['kind'] == 'shop':
            indicador_ids = [l.shop.id for l in self.lines]
            where &= shop.id.in_(indicador_ids)
            columns = [shop.name.as_('indicador'), shop.id.as_('indicador_id')]
            group_by = (shop.name, shop.id)
        elif data['kind'] == 'category':
            pass
            # from_items.join(tem_cat, condition=template.id==tem_cat.template
            # ).join(prod_cat, condition=tem_cat.category == prod_cat.id)
            # indicador_ids = [l.category.id for l in self.lines]
            # where &= prod_cat.id.in_(indicador_ids)
            # columns = [prod_cat.name.as_('indicador'), prod_cat.id.as_('indicador_id')]
            # group_by = (prod_cat.name, prod_cat.id)

        where &= invoice.invoice_date <= data['to_date']
        if data['period'] == 'month':
            year = self.start_date.year
            _range = TYPE_DICT[data['type']]['range']
            for p in range(0, _range, 1):
                month = p + 1
                _, end_day = calendar.monthrange(year, month)
                start_date = date(year, month, 1)
                end_date = date(year, month, end_day)
                condition_case = ((invoice.invoice_date >= start_date) & (invoice.invoice_date <= end_date))
                str_date = lang.strftime(start_date, format="%y-%b")
                if data['with_tax'] and data['kind'] in ['shop', 'salesman']:
                    columns.extend([Sum(Case((condition_case, invoice.untaxed_amount_cache + with_invoicetax.amount), else_=0)).as_(str_date)])
                else:
                    columns.extend([Sum(Case((condition_case, line.quantity * line.unit_price), else_=0)).as_(start_date.strftime("%y-%b"))])

        columns.extend([column_amount.as_('Total')])

        query = from_items.select(
            *columns,
            where=where,
            group_by=group_by,
            with_=with_,
        )
        return query


class GoalIndicator(ModelSQL, ModelView):
    "Goal Indicator"
    __name__ = 'sale.goal.indicator'
    place = fields.Many2One('sale.goal.place', 'Place', states={
        'required': Bool(Not(Eval('parent'))),
    })
    indicator = fields.Selection(INDICATORS, 'Indicator', required=True)
    salesman = fields.Many2One('company.employee', 'Salesman', states={
        'invisible': Eval('indicator') != 'salesman',
        'required': Eval('indicator') == 'salesman',
    })
    product = fields.Many2One('product.product', 'Product', domain=[
            ('template.type', 'in', ['goods', 'services']),
            ('template.salable', '=', True),
        ], states={
            'invisible': Eval('indicator') != 'product',
            'required': Eval('indicator') == 'product',
        })
    category = fields.Many2One('product.category', 'Category', states={
        'invisible': Eval('indicator') != 'category',
        'required': Eval('indicator') == 'category',
    })
    total_amount = fields.Numeric('Total Amount', digits=(16, 2), required=True)
    childs = fields.One2Many('sale.goal.indicator', 'parent', 'Childs')
    parent = fields.Many2One('sale.goal.indicator', 'Parent')

    @staticmethod
    def default_total_amount():
        return 0


class SaleGoalReport(Report):
    "Sale Goal Report"
    __name__ = 'sale.goal.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Goal = pool.get('sale.goal')
        total_amounts = []

        lines = {}
        for obj in records:
            report_context.update(obj.get_periods_zeros(obj.fiscalyear))
            for line in obj.lines:
                if line.indicator.id not in lines:
                    lines[line.indicator.id] = {
                        'name': line.indicator.name,
                        'total': _ZERO,
                    }
                    lines[line.indicator.id].update(Goal.get_periods_zeros(obj.fiscalyear))
                month = line.period.name[-2:]
                lines[line.indicator.id][month] = line.amount
                lines[line.indicator.id]['total'] += line.amount
                data[month] += line.amount
                total_amounts.append(line.amount)
            report_context['fiscalyear'] = obj.fiscalyear.name

        report_context['data'] = data
        report_context['sum_total_amount'] = sum(total_amounts)
        report_context['objects'] = lines.values()
        return report_context


class SaleGoalAchievement(Wizard):
    "Sale Goal Achievement"
    __name__ = "sale_goal.achievement"

    start = StateView('goal.reporting.context',
        'sale_goal.goal_reporting_context_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('sale_goal.achievement.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
            'period': self.start.period,
            'type': self.start.type,
            'kind': self.start.kind,
            'with_tax': self.start.with_tax,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleGoalAchievementReport(Report):
    "Sale goal Achievement Report"
    __name__ = "sale_goal.achievement.report"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        GoalLine = pool.get('sale.goal.line')
        domain = [
            ('kind', '=', data['kind']),
            ('type', '=', data['type']),
        ]
        domain.append(get_dom_goal_period(data['from_date'], data['to_date']))
        goal_lines = GoalLine.search(domain)
        records = []
        for line in goal_lines:
            achievement = {}
            fixed_amount = line.fixed_amount
            for l in line.lines:
                amount = 0
                if l.amount:
                    amount = l.amount
                elif fixed_amount:
                    amount = fixed_amount
                indicador = getattr(l, data['kind'])
                if indicador:
                    achievement[indicador.id] = amount
            with Transaction().set_context({'data': data}):
                query = line.get_sales_query()

            cursor = Transaction().connection.cursor()
            cursor.execute(*query)
            header_col = list(col.name for col in cursor.description)
            values, totals = cls.get_values(cursor.fetchall(), header_col, achievement)
            header_col.extend(['meta', 'cumplimiento'])
            header_col.remove('indicador_id')
            value = {
                'line': line,
                'achievement': achievement,
                'lines': values,
                'header': header_col,
                'totals': totals,
            }
            records.append(value)

        report_context['records'] = records
        return report_context

    @classmethod
    def get_values(cls, result, header_col, achievement):
        records = []
        totals = {}
        records_append = records.append
        for row in result:
            row_dict = {}
            for i, col in enumerate(header_col):
                if type(row[i]) == float:
                    row_dict[col] = f'{round(row[i], 2):,.2f}'
                else:
                    row_dict[col] = row[i]
                if not col.startswith('indicador'):
                    try:
                        totals[col] = float(str(round(totals[col] + row[i], 2)))
                    except:
                        totals[col] = row[i]
                elif col == 'indicador':
                    totals[col] = 'Total:'
            row_dict['Meta'] = achievement[row_dict['indicador_id']]
            del row_dict['indicador_id']
            try:
                row_dict['Cumplimiento'] = '{:,.2f}%'.format(float(row_dict['Total']) / float(row_dict['Meta']) * 100)
            except:
                row_dict['Cumplimiento'] = f'{0.00:,.2f}%'
            records_append(row_dict.values())
        return records, totals.values()
